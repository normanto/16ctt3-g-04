#include "VARIABLE.h"

VARIABLE::VARIABLE() {
}
VARIABLE::VARIABLE(const VARIABLE &aVariable) {
	name = aVariable.name;
	exponent = aVariable.exponent;
}
VARIABLE::VARIABLE(char aName, int aExp) {
	name = aName;
	exponent = aExp;
}
VARIABLE::~VARIABLE() = default;
char VARIABLE::getName()const {
	return name;
}
int VARIABLE::getExponent()const {
	return exponent;
}
void VARIABLE::setName(char aName) {
	name = aName;
}
void VARIABLE::setExponent(int aExp) {
	this->exponent = aExp;
}
bool VARIABLE::operator== (const VARIABLE &aVariable) const {
	return ((name == aVariable.name) && (exponent == aVariable.exponent));
}
bool VARIABLE::operator!= (const VARIABLE &aVariable) const {
	return !(*this == aVariable);
}
bool VARIABLE::operator< (const VARIABLE &aVariable) const {
	if (name == aVariable.name)
		return exponent < aVariable.exponent;
	else if (name < aVariable.name) return true;// a<b
	return false;
}
bool VARIABLE:: operator<= (const VARIABLE &aVariable) const {
	return (*this < aVariable) || (*this == aVariable);
}
istream& operator >> (istream &is, VARIABLE &aVariable) {
	char tmp;
	is >> aVariable.name >> tmp;
	if (is.eof()) {
		aVariable.exponent = 1;
		return is;
	}
	if (tmp == '^') is >> aVariable.exponent;
	else {
		is.seekg(-1, ios::cur);
		aVariable.exponent = 1;
	}
	return is;
}
ostream& operator << (ostream &os, const VARIABLE &aVariable) {
	os << aVariable.getName();
	if (aVariable.getExponent() != 1)
		os << '^' << aVariable.getExponent();
	return os;
}