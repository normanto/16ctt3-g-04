#ifndef _LINKED_LIST_H
#define _LINKED_LIST_H

template <class T> class LINKED_LIST {
private:
	struct ListNode {
		T data; //data of node
		ListNode *next; //point to next node
	};
	int size; //number of node in list
	ListNode *head; //point to 1st node in list
	void merge(ListNode*&, ListNode*, ListNode*);
	void mergeSort(ListNode*&);
public:
	LINKED_LIST(); //default contructor
	LINKED_LIST(const LINKED_LIST&); //copy contructor
	~LINKED_LIST(); //destructor
	bool isEmpty();
	int getLength() const;
	bool insert(int, T); //insert after index
	bool remove(int);
	int findNode(T); //return node index or -1
	bool retrieve(int, T&) const;
	bool replace(int, T);
	void sortAscending();
	bool operator== (const LINKED_LIST&) const;
	LINKED_LIST<T>& operator= (const LINKED_LIST&);
	LINKED_LIST<T>& operator+= (const LINKED_LIST&);
}; //end class

template <class T> LINKED_LIST<T>::LINKED_LIST() {
	head = nullptr;
	size = 0;
}
template <class T> LINKED_LIST<T>::LINKED_LIST(const LINKED_LIST &aList) {
	size = 0;
	ListNode *curr = aList.head;
	for (int i = 0; curr != nullptr; curr = curr->next, i++)
		this->insert(i, curr->data);
}
template <class T> LINKED_LIST<T>::~LINKED_LIST() {
	while (!isEmpty()) remove(0);
}
template <class T> bool LINKED_LIST<T>::isEmpty() {
	return (head == nullptr);
}
template <class T> int LINKED_LIST<T>::getLength()const {
	return size;
}
template <class T> bool LINKED_LIST<T>::insert(int index, T newItem) {
	if ((index < 0) || (index > size)) return false;
	ListNode* node = new ListNode;
	node->data = newItem;
	node->next = nullptr;
	size++;
	if (index == 0) {
		if (head == nullptr)
			head = node;
		else {
			node->next = head;
			head = node;
		}
	}
	else {
		ListNode *curr = head;
		int pos = 0;
		while ((curr != nullptr) && (pos != index - 1)) {
			curr = curr->next;
			pos++;
		}
		node->next = curr->next;
		curr->next = node;
	}
	return true;
}
template <class T> bool LINKED_LIST<T>::remove(int index) {
	if ((index < 0) || (index >= size)) return false;
	size--;
	if (index == 0) {
		ListNode *tmp = head;
		head = head->next;

		if (tmp != nullptr) {
			delete tmp;
			tmp = nullptr;
		}
	}
	else {
		ListNode *curr = head;
		int pos = 0;
		while ((curr != nullptr) && (pos != index - 1)) {
			curr = curr->next;
			pos++;
		}
		ListNode *tmp = curr->next;
		curr->next = tmp->next;
		tmp->next = nullptr;
		delete tmp;
		tmp = nullptr;
	}
	return true;
}
template <class T> int LINKED_LIST<T>::findNode(T key)
{
	int pos = -1;
	ListNode *curr = head;
	while (curr != nullptr) {
		pos++;
		if (curr->data == key) return pos;
		curr = curr->next;
	}
	return -1;
}
template <class T> bool LINKED_LIST<T>::retrieve(int index, T &itemData) const {
	if ((index < 0) || (index >= size)) return false;
	int pos = -1;
	ListNode *curr = head;
	while (curr != nullptr) {
		pos++;
		if (pos == index) break;
		curr = curr->next;
	}
	itemData = curr->data;
	return true;
}
template <class T> bool LINKED_LIST<T>::replace(int index, T itemData) {
	if ((index < 0) || (index > size)) return false;
	remove(index);
	insert(index, itemData);
	return true;
}
template <class T> void LINKED_LIST<T>::merge(ListNode *&dest, ListNode *src1, ListNode *src2) {
	ListNode *curr;
	if (src1->data <= src2->data) {
		dest = src1;
		src1 = src1->next;
	}
	else {
		dest = src2;
		src2 = src2->next;
	}
	curr = dest;
	while (src1 && src2) {
		if (src1->data <= src2->data) {
			curr->next = src1;
			src1 = src1->next;
		}
		else {
			curr->next = src2;
			src2 = src2->next;
		}
		curr = curr->next;
	}
	curr->next = (src1 ? src1 : src2);
}
template <class T> void LINKED_LIST<T>::mergeSort(ListNode *&list) {
	if (!list || !list->next) return;
	ListNode* list1 = list;
	ListNode* list2 = list->next;
	list->next = nullptr;
	mergeSort(list1);
	mergeSort(list2);
	merge(list, list1, list2);
}
template <class T> void LINKED_LIST<T>::sortAscending() {
	if (isEmpty()) return;
	mergeSort(head);
}
template <class T> bool LINKED_LIST<T>::operator== (const LINKED_LIST &aList) const {
	int lengthCompare = this->size < aList.size ? this->size : aList.size;
	int i;
	for (i = 0; i < lengthCompare; i++) {
		T tmp1;
		T tmp2;
		this->retrieve(i, tmp1);
		aList.retrieve(i, tmp2);
		if (tmp1 != tmp2) return false;
	}
	return (i == this->size) && (this->size == aList.size);
}
template <class T> LINKED_LIST<T>& LINKED_LIST<T>::operator= (const LINKED_LIST &aList) {
	while (!isEmpty()) remove(0);
	ListNode *curr = aList.head;
	for (int i = 0; curr != nullptr; curr = curr->next, i++)
		this->insert(i, curr->data);
	return (*this);
}
template <class T> LINKED_LIST<T>& LINKED_LIST<T>::operator+= (const LINKED_LIST &aList) {
	ListNode *curr = aList.head;
	for (int i = this->size; curr != nullptr; curr = curr->next, i++)
		this->insert(i, curr->data);
	return (*this);
}
#endif