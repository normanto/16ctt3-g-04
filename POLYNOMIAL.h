﻿#ifndef _POLYNOMIAL_H
#define _POLYNOMIAL_H
#include "TERM.h"

class POLYNOMIAL{
private :
	LINKED_LIST<TERM> terms;
public:
	POLYNOMIAL();
	POLYNOMIAL(const POLYNOMIAL&);
	~POLYNOMIAL();
	POLYNOMIAL& operator~ ();//Chuẩn hóa đa thức <xóa đơn thức 0>, chuẩn hóa đơn thức trùng
	void sortAscending();//Sắp xếp đa thức
	POLYNOMIAL& operator= (const POLYNOMIAL&);
	POLYNOMIAL operator+ (const POLYNOMIAL&);
	POLYNOMIAL operator- (const POLYNOMIAL&);
	POLYNOMIAL operator* (const POLYNOMIAL&);
	friend istream& operator >> (istream&, POLYNOMIAL&);
	friend ostream& operator << (ostream&, const POLYNOMIAL&);
};

#endif