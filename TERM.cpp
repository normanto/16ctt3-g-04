﻿#include "TERM.h"

TERM::TERM() {
	coefficient = 0;
}
TERM::TERM(const TERM &aTerm)
{
	coefficient = aTerm.getCoefficient();
	int size = aTerm.variables.getLength();
	variables = aTerm.variables;
}
TERM::~TERM()
{
	if (!variables.isEmpty()) {
		coefficient = 0;
		while (!variables.isEmpty())
			variables.remove(0);
	}
}
double TERM::getCoefficient() const {
	return coefficient;
}
void TERM::setCoefficient(double aCoeff) {
	coefficient = aCoeff;
}
int TERM::getSumExponent() const {
	int sum = 0;
	int size = variables.getLength();
	for (int i = 0; i < size; i++) {
		VARIABLE tmp;
		variables.retrieve(i, tmp);
		sum += tmp.getExponent();
	}
	return sum;
}
void TERM::sortAscending() {
	variables.sortAscending();
}
TERM& TERM::operator~ () {
	//rút gọn 2*a^3*a = 2*a^4
	int size = variables.getLength();
	VARIABLE currOne, currTwo;
	for (int i = 0; i < size; i++) {
		if (variables.retrieve(i, currOne)) {
			for (int j = i + 1; j < size; j++) {
				if (variables.retrieve(j, currTwo)) {
					if (currOne.getName() == currTwo.getName()) {
						currOne.setExponent(currOne.getExponent() + currTwo.getExponent());
						variables.remove(j);
						j--;
						size--;
					}
				}
			}
			variables.replace(i, currOne);
		}
	}
	return *this;
}
TERM TERM::operator* (const TERM &aTerm) {
	TERM result;
	result.setCoefficient(this->getCoefficient() * aTerm.getCoefficient());
	result.variables = this->variables;
	result.variables += aTerm.variables;
	~result;
	return result;
}
bool TERM::operator== (const TERM &aTerm) {
	if (this->coefficient != aTerm.coefficient) return false;
	if (this->variables.getLength() != aTerm.variables.getLength()) return false;
	return (this->variables == aTerm.variables);
}
bool TERM::operator< (const TERM &aTerm) {
	if (this->variables.getLength() == 0) return true;
	else if (aTerm.variables.getLength() == 0) return false;

	if (this->getSumExponent() < aTerm.getSumExponent()) return true;
	else if (this->getSumExponent() > aTerm.getSumExponent()) return false;

	int length_This = this->variables.getLength();
	int length_aTerm = aTerm.variables.getLength();
	int lenghtCompare = length_This < length_aTerm ? length_This : length_aTerm;
	for (int i = 0; i < lenghtCompare; i++) {
		VARIABLE tmp1, tmp2;
		this->variables.retrieve(i, tmp1);
		aTerm.variables.retrieve(i, tmp2);
		if (tmp1 == tmp2) continue;
		else if (tmp1 < tmp2) {
			if ((i == length_aTerm - 1) && (i < length_This - 1)) return false;// 2*a^2 đứng trước 3*a*b cho hợp lý
			else return true;
		}
		return false;
	}
	//Các đơn thức == nhau so sánh hệ số
	if (this->coefficient > aTerm.coefficient) return false;
	if (this->coefficient < aTerm.coefficient) return true;
}
bool TERM::operator<= (const TERM &aTerm) {
	return (*this < aTerm) || (*this == aTerm);
}
bool TERM::variablesSame(const TERM &aTerm) {
	return (this->variables == aTerm.variables);
}
istream& operator >> (istream &is, TERM &Term) {
	char tmpOperator;
	is >> tmpOperator;
	if (((tmpOperator >= 'A') && (tmpOperator <= 'Z')) || ((tmpOperator >= 'a') && (tmpOperator <= 'z'))) {
		Term.coefficient = 1;
		is.seekg(-1, ios::cur);
	}
	else {
		is >> tmpOperator;
		if (((tmpOperator >= 'A') && (tmpOperator <= 'Z')) || ((tmpOperator >= 'a') && (tmpOperator <= 'z'))) {
			Term.coefficient = -1;
			is.seekg(-1, ios::cur);
		}
		else {
			is.seekg(-2, ios::cur);
			is >> Term.coefficient;
		}
	}
	if ((Term.coefficient == 1) || (Term.coefficient == -1)) {
		int i = 0;
		while (!is.eof())
		{
			VARIABLE tmpVariable;
			is >> tmpVariable;
			Term.variables.insert(i, tmpVariable);
			i++;
			if (is.eof()) break;
			is >> tmpOperator;
			if (tmpOperator != '*') {
				is.seekg(-1, ios::cur);
				break;
			}
		}
	}
	else {
		is >> tmpOperator;
		if (tmpOperator == '*') {
			int i = 0;
			while (!is.eof())
			{
				VARIABLE tmpVariable;
				is >> tmpVariable;
				Term.variables.insert(i, tmpVariable);
				i++;
				if (is.eof()) break;
				is >> tmpOperator;
				if (tmpOperator != '*') {
					is.seekg(-1, ios::cur);
					break;
				}
			}
		}
	}
	return is;
}
ostream& operator << (ostream& os, const TERM &Term) {
	if (Term.getCoefficient() != 1 && Term.getCoefficient() != -1)
		os << Term.getCoefficient() << '*';
	if (Term.getCoefficient() == -1) os << '-';
	for (int i = 0; i < Term.variables.getLength(); i++) {
		if (i != 0) os << '*';
		VARIABLE tmp;
		Term.variables.retrieve(i, tmp);
		os << tmp;
	}
	return os;
}
TERM& TERM::operator = (const TERM &aTerm)
{
	coefficient = aTerm.coefficient;
	int size = aTerm.variables.getLength();
	VARIABLE tmp;
	for (int i = 0; i < size; i++) {
		aTerm.variables.retrieve(i, tmp);
		variables.insert(i, tmp);
	}
	return (*this);
}