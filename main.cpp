﻿#include "POLYNOMIAL.h"
#include <fstream>

void main()
{
	POLYNOMIAL F1, F2, F3, F4, F5;

	fstream f1("F1.txt", ios::in);
	if (f1.fail()) return;
	f1 >> F1;
	~F1;//Chuẩn hóa đa thức
	F1.sortAscending();//Sắp xếp đa thức
	f1.close();

	fstream f2("F2.txt", ios::in);
	if (f2.fail()) return;
	f2 >> F2;
	~F2;
	F2.sortAscending();
	f2.close();

	F3 = F1 + F2;
	~F3; F3.sortAscending();
	F4 = F1 - F2;
	~F4; F4.sortAscending();
	F5 = F1 * F2;
	~F5; F5.sortAscending();

	fstream fOutput("F-result.txt", ios::out);
	if (fOutput.fail()) return;
	fOutput << "F1" << endl << F1 << endl;
	fOutput << "F2" << endl << F2 << endl;
	fOutput << "F3" << endl << F3 << endl;
	fOutput << "F4" << endl << F4 << endl;
	fOutput << "F5" << endl << F5;
	fOutput.close();
}