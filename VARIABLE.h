﻿#ifndef _VARIABLE_H
#define _VARIABLE_H
#include <iostream>
using namespace std;

class VARIABLE {
	char name;
	int exponent;
public:
	VARIABLE();
	VARIABLE(char, int);
	VARIABLE(const VARIABLE&);
	~VARIABLE();
	char getName()const;
	int getExponent()const;
	void setExponent(int);
	void setName(char);
	bool operator== (const VARIABLE&) const;
	bool operator!= (const VARIABLE&) const;
	bool operator< (const VARIABLE&) const;
	bool operator<= (const VARIABLE&) const;//dùng để sort danh sách biến
	friend istream& operator >> (istream&, VARIABLE&);
	friend ostream& operator << (ostream&, const VARIABLE&);
};

#endif