﻿#include "POLYNOMIAL.h"

POLYNOMIAL::POLYNOMIAL(){
}
POLYNOMIAL::POLYNOMIAL(const POLYNOMIAL &aPolynomial) {
	this->terms = aPolynomial.terms;
}
POLYNOMIAL::~POLYNOMIAL() = default;
POLYNOMIAL& POLYNOMIAL::operator~ () {
	//Xử lý các đơn thức trùng biến 
	int size = terms.getLength();
	for (int i = 0; i < size; i++) {
		TERM currOne;
		if (terms.retrieve(i, currOne)) {
			currOne.sortAscending();
			for (int j = i + 1; j < size; j++) {
				TERM currTwo;
				if (terms.retrieve(j, currTwo)) {
					currTwo.sortAscending();
					if (currOne.variablesSame(currTwo)) {//variables như nhau
						currOne.setCoefficient(currOne.getCoefficient() + currTwo.getCoefficient());
						terms.remove(j);
						j--;
						size--;
					}
				}
			}
			terms.replace(i, currOne);
		}
	}
	//Xóa đơn thức 0
	for (int i = 0; i < terms.getLength(); i++) {
		TERM tmp;
		terms.retrieve(i, tmp);
		if (tmp.getCoefficient() == 0){
			terms.remove(i);
			i--;
		}
	}
	return (*this);
}
void POLYNOMIAL::sortAscending() {
	this->terms.sortAscending();
}
POLYNOMIAL& POLYNOMIAL::operator= (const POLYNOMIAL &aPolynomial) {
	this->terms = aPolynomial.terms;
	return (*this);
}
POLYNOMIAL POLYNOMIAL::operator+ (const POLYNOMIAL &aPolynomial) {
	POLYNOMIAL result;
	result.terms = this->terms;
	result.terms += aPolynomial.terms;
	~result; 
	result.sortAscending();
	return result;
}
POLYNOMIAL POLYNOMIAL::operator- (const POLYNOMIAL &aPolynomial){
	POLYNOMIAL tmp_aPoly;
	tmp_aPoly.terms = aPolynomial.terms;
	int size = aPolynomial.terms.getLength();
	for (int i = 0; i < size; i++){
		TERM tmp;
		tmp_aPoly.terms.retrieve(i, tmp);
		tmp.setCoefficient(-tmp.getCoefficient());
		tmp_aPoly.terms.replace(i, tmp);
	}
	POLYNOMIAL result = *this + tmp_aPoly;
	~result;
	result.sortAscending();
	return result;
}
POLYNOMIAL POLYNOMIAL::operator* (const POLYNOMIAL &aPolynomial){
	POLYNOMIAL *arr = new POLYNOMIAL[this->terms.getLength()];
	POLYNOMIAL result;
	for (int i = 0; i < this->terms.getLength(); i++) {
		TERM currThis;
		if (this->terms.retrieve(i, currThis)) {
			for (int j = 0; j < aPolynomial.terms.getLength(); j++) {
				TERM curr_aPoly;
				if (aPolynomial.terms.retrieve(j, curr_aPoly)) {
					TERM tmp = currThis * curr_aPoly;
					arr[i].terms.insert(j, tmp);
				}
			}
		}
	}
	for (int i = 0; i < this->terms.getLength(); i++) {
		result = result + arr[i];
	}
	~result;
	result.sortAscending();
	return result;
}
istream& operator >> (istream &is, POLYNOMIAL &Polynamial) {
	int i = 0;
	while (!is.eof())
	{
		TERM tmpTerm;
		char tmpOpeator;//+-
		is >> tmpTerm;
		Polynamial.terms.insert(i, tmpTerm);
		if (is.eof()) break;
		is >> tmpOpeator;
		if (tmpOpeator == '-') is.seekg(-1, ios::cur);
		i++;

	}
	return is;
}
ostream& operator << (ostream &os, const POLYNOMIAL &Polynamial) {
	for (int i = 0; i < Polynamial.terms.getLength(); i++) {
		TERM tmp;
		Polynamial.terms.retrieve(i, tmp);
		if (i != 0)
			if (tmp.getCoefficient() > 0) os << '+';
		os << tmp;
	}
	return os;
}