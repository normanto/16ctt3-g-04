﻿#ifndef _TERM_H
#define _TERM_H

#include "LINKED_LIST.h"
#include "VARIABLE.h"

class TERM{
private:
	double coefficient;
	LINKED_LIST<VARIABLE> variables;
public:
	TERM();
	TERM(const TERM&);
	~TERM();
	double getCoefficient()const;
	void setCoefficient(double);
	int getSumExponent()const;
	void sortAscending();
	TERM& operator~();// Rút gọn đơn thức 2*a^2*a^3 = 2*a^5
	TERM operator* (const TERM&);
	bool operator== (const TERM&);
	bool operator< (const TERM&);
	bool operator<= (const TERM&);//dùng để sort danh sách đơn thức
	bool variablesSame(const TERM&);
	TERM& operator = (const TERM &);
	friend istream& operator >> (istream&, TERM&);
	friend ostream& operator << (ostream&, const TERM&);
};

#endif